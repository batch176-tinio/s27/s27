
let http = require("http");


let courses = [
	{
		name: "English 101",
		price: 23000,
		isActive: true,
	},
	{
		name: "Math 101",
		price: 25000,
		isActive: true,
	},
	{
		name: "Reading 101",
		price: 21000,
		isActive: true,
	}

];

http.createServer(function(req, res){

	// HTTP methods allow us to identify what action take for our request.
	// With an HTTP method, we can actually provide routes that cater to same endpoint but with different action.
	// Most HTTP methods are primarily concerned with CRUD operations
	// Common HTTP methods:
	// GET - retrieve or get data
	// POST - indicates that client wants to send data to create resource
	// PUT - indicates that client wants to send data to update resource
	// DELETE - indicates that client wants to send data to delete resource

	console.log(req.url) // the request URL endpoint
	console.log(req.method) // the method of the request

	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a GET method request")
	} else if(req.url === "/" && req.method === "POST") {
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a POST method request");
	} else if(req.url === "/" && req.method === "PUT"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a PUT method request");
	} else if(req.url === "/" && req.method === "DELETE"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a DELETE method request");
	} else if(req.url === "/courses" && req.method === "GET") {
		// since we are now passing a stringified JSON, so that the client
		// which will receive our response be able to display our data properly,
		// we have to update our content-type header to application/json
		res.writeHead(200,{'Content-Type':'application/json'});
		// we cannot pass another data type other than a string for our end()
		// So, to be able to pass our array, we have to transform into a JSON string.
		res.end(JSON.stringify(courses));

	} else if(req.url === "/courses" && req.method === "POST"){

		/*This route should be able to receive data from the client and we
		should be able to create a new course and add it to our courses array.
		*/

		// This will act as a placeholder to contain the data passed from the client.
		let requestBody = "";

		/*Receiving data from a client to a nodejs server requires 2 steps:

		1. data step - this part will read the stream of data coming from our client
		and process the incoming data into the requestBody variable.
		*/

		req.on('data',function(data){

			// console.log(data);  // stream of data from client 
			requestBody += data // data stream is saved into the variable

		})

		/*
		2. end step - this will run once or after the request data has been completely
		sent from our client.
		*/

		req.on('end',function(){

			// console.log(requestBody);
			// requestBody now contains data from our Postman Client

			// Since requestBody is in JSON format we have to parse to add
			// it as an object in our array.


			requestBody = JSON.parse(requestBody);


			// Simulate creating a document and add it in a collection:
			let newCourse = {

				// we are getting the value of name and price from requestBody object.
				name: requestBody.name,
				price: requestBody.price,
				isActive: true

			}

			console.log(newCourse);

			courses.push(newCourse);
			console.log(courses);

			res.writeHead(200,{'Content-Type':'application/json'});
			res.end(JSON.stringify(courses));

		})




		
	}

}).listen(4000);

console.log("Server is running at localhost:4000");

